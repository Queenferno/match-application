﻿using MatchApplication.DAL;
using MatchApplication.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace MatchApplication.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MatchController : ControllerBase
    {      
        private readonly ILogger<MatchController> _logger;
        private readonly IDbContext _dbContext;

        public MatchController(ILogger<MatchController> logger, IDbContext dbContext)
        {
            _logger = logger;
            _dbContext = dbContext;
        }

        /// <summary>
        /// Returns a list of Matches and their Odds
        /// </summary>
        /// <returns>A list of all saved Matches</returns>
        [HttpGet]
        public async Task<ActionResult<List<MatchDTO>>> Get()
        {
            var data = await _dbContext.MatchSet.Include(x => x.Odds).ToListAsync(); 
            var dto = data.Select(x => new MatchDTO(x)).ToList();
            
            return dto; 
        }

        /// <summary>
        /// Finds and retrieves a Match, based on its [id]. If not found, returns a NotFound result
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MatchDTO>> Get(int id)
        {
            var data = await _dbContext.MatchSet.Include(x => x.Odds).FirstOrDefaultAsync(x => x.ID == id);

            if (data == null) return new NotFoundObjectResult(id);

            return new MatchDTO(data);
        }

        /// <summary>
        /// Updates a saved Match and its Odds, by [id]. The id and the match.ID must be identical, otherwise a BadRequest is returned.
        /// </summary>
        /// <param name="id">The ID of the match to be updated</param>
        /// <param name="match">The Match object whose values are to be used in the update</param>
        /// <param name="cancellationToken"></param>
        /// <returns>
        /// OK if everything runs smoothly, BadRequest if the ID and the match.ID are different, NotFound if the match is not
        /// found in the DB. Error 500 if an exception is thrown 
        /// </returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, Match match, CancellationToken cancellationToken)
        {
            try
            {
                if (id != match.ID)
                {
                    return BadRequest();
                }

                var savedMatch = await _dbContext.MatchSet.Include(x => x.Odds).FirstOrDefaultAsync(x => x.ID == id);

                if (savedMatch == null)
                {
                    return NotFound();
                }


                savedMatch.Description = match.Description;
                savedMatch.MatchDate = match.MatchDate;
                savedMatch.MatchTime = match.MatchTime;
                savedMatch.Sport = match.Sport;
                savedMatch.TeamA = match.TeamA;
                savedMatch.TeamB = match.TeamB;

                var deletedOddsIDs = savedMatch.Odds?.Select(x => x.ID).Except(match.Odds?.Select(x => x.ID));
                if (deletedOddsIDs?.Any() == true)
                {
                    foreach (var deletedOddID in deletedOddsIDs)
                    {
                        var odd = await _dbContext.MatchOddSet.FindAsync(deletedOddID);
                        _dbContext.MatchOddSet.Remove(odd);
                    }
                }

                savedMatch.Odds = match.Odds;

                await _dbContext.SaveChangesAsync(cancellationToken);
                return Ok();
            }
            catch (Exception e)
            {
                _logger.LogError($"Failed to update match [{id}]", e);
                return new StatusCodeResult(500);
            }
        }

        /// <summary>
        /// Creates a new Match and adds its Odds
        /// </summary>
        /// <param name="match">The match and its odds to be added</param>
        /// <param name="cancellationToken"></param>
        /// <returns>OK if the addition was successful, error 500 otherwise </returns>
        [HttpPost]
        public async Task<IActionResult> Post(Match match, CancellationToken cancellationToken)
        {
            try
            {
                _dbContext.MatchSet.Add(match);
                await _dbContext.SaveChangesAsync(cancellationToken);
                return Ok();
            }
            catch (Exception e)
            {
                _logger.LogError("Failed to save a new Match", e);
                return new StatusCodeResult(500);
            }
        }

        /// <summary>
        /// Deletes a saved Match and its Odds, by [id]
        /// </summary>
        /// <param name="id">The ID of the match to be deleted</param>
        /// <param name="cancellationToken"></param>
        /// <returns>NotFound if the match is not found in the DB, otherwise an OK</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id, CancellationToken cancellationToken)
        {
            try
            {
                var match = await _dbContext.MatchSet.FindAsync(id);
                if (match == null) return NotFound();

                _dbContext.MatchSet.Remove(match);
                await _dbContext.SaveChangesAsync(cancellationToken);

                return Ok();
            }
            catch (Exception e)
            {
                _logger.LogError($"Failed to delete Match [{id}]", e);
                return new StatusCodeResult(500);
            }
        }
    }
}
