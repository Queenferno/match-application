﻿using MatchApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MatchApplication.DTOs
{  
    public class OddsDTO
    {
        public int ID { get; set; }
        public string Specifier { get; set; }
        public double Odd { get; set; }

        public OddsDTO(MatchOdd odd)
        {
            if (odd == null) return;

            ID = odd.ID;
            Specifier = odd.Specifier;
            Odd = odd.Odd;        
        }
    }
}
