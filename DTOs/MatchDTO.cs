﻿using MatchApplication.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MatchApplication.Models
{
    public class MatchDTO
    {

        public int ID { get; set; }
        public string Description { get; set; }
        public DateTime MatchDate { get; set; }
        public DateTime MatchTime { get; set; }
        public string TeamA { get; set; }
        public string TeamB { get; set; }
        public Sport Sport { get; set; }
        public List<OddsDTO> Odds { get; set; }

        public MatchDTO (Match match)
        {
            if (match == null) return;
            ID = match.ID;
            Description = match.Description;
            MatchDate = match.MatchDate;
            MatchTime = match.MatchTime;
            TeamA = match.TeamA;
            TeamB = match.TeamB;
            Sport = match.Sport;
            Odds = match.Odds == null
                ? new List<OddsDTO>()
                : match.Odds.Select(x => new OddsDTO(x)).ToList();
        }
    }   
}
