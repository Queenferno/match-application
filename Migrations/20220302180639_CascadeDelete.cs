﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MatchApplication.Migrations
{
    public partial class CascadeDelete : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MatchOddSet_MatchSet_MatchID",
                table: "MatchOddSet");

            migrationBuilder.AddColumn<int>(
                name: "MatchID1",
                table: "MatchOddSet",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_MatchOddSet_MatchID1",
                table: "MatchOddSet",
                column: "MatchID1");

            migrationBuilder.AddForeignKey(
                name: "FK_MatchOddSet_MatchSet_MatchID",
                table: "MatchOddSet",
                column: "MatchID",
                principalTable: "MatchSet",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MatchOddSet_MatchSet_MatchID1",
                table: "MatchOddSet",
                column: "MatchID1",
                principalTable: "MatchSet",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MatchOddSet_MatchSet_MatchID",
                table: "MatchOddSet");

            migrationBuilder.DropForeignKey(
                name: "FK_MatchOddSet_MatchSet_MatchID1",
                table: "MatchOddSet");

            migrationBuilder.DropIndex(
                name: "IX_MatchOddSet_MatchID1",
                table: "MatchOddSet");

            migrationBuilder.DropColumn(
                name: "MatchID1",
                table: "MatchOddSet");

            migrationBuilder.AddForeignKey(
                name: "FK_MatchOddSet_MatchSet_MatchID",
                table: "MatchOddSet",
                column: "MatchID",
                principalTable: "MatchSet",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
