﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MatchApplication.Models
{
    public class MatchOdd
    {
        public int ID { get; set; }
        public string Specifier { get; set; }
        public double Odd { get; set; }
        public Match Match { get; set; }
    }
}
