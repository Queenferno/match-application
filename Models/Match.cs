﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MatchApplication.Models
{
    public class Match
    {
        public int ID { get; set; }
        public string Description { get; set; }
        public DateTime MatchDate { get; set; }
        public DateTime MatchTime { get; set; }
        public string TeamA { get; set; }
        public string TeamB { get; set; }
        public Sport Sport { get; set; }
        public ICollection<MatchOdd> Odds { get; set; }
    }

    public enum Sport
    {
        Football, 
        Basketball
    }
}
