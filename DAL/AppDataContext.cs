﻿using MatchApplication.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MatchApplication.DAL
{
    public class AppDataContext : DbContext, IDbContext
    {
        private readonly IHostEnvironment env;

        public AppDataContext(IHostEnvironment env) : base()
        {
            this.env = env;
        }

        public DbSet<Models.Match> MatchSet { get; set; }
        
        public DbSet<MatchOdd> MatchOddSet { get; set; }
        

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

            IConfigurationRoot configuration = new ConfigurationBuilder()
            .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
            .AddJsonFile("appsettings.json")
            .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true)
            .Build();

            var connectionString = configuration.GetValue<string>("ConnectionString");
            optionsBuilder.UseSqlServer(connectionString);            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Models.Match>().HasMany(x => x.Odds).WithOne().OnDelete(DeleteBehavior.Cascade);
        }
    }
}
