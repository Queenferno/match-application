﻿using MatchApplication.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MatchApplication.DAL
{
    public interface IDbContext
    {
        DbSet<Match> MatchSet { get; set; }

        DbSet<MatchOdd> MatchOddSet { get; set; }
        

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
        DbSet<T> Set<T>() where T : class;
        int SaveChanges();
    }
}
